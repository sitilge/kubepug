FROM alpine as build

RUN apk add --no-cache curl

RUN curl -LO https://github.com/rikatz/kubepug/releases/download/v1.3.3/kubepug_linux_amd64.tar.gz

RUN tar xvf kubepug_linux_amd64.tar.gz

FROM alpine

RUN apk add --no-cache curl

COPY --from=build kubepug /usr/bin/kubepug

#Harden the image
ARG name=nonroot
ARG id=12345

RUN addgroup -g ${id} -S ${name} && adduser -u ${id} -S -G ${name} -h /home/${name} ${name}

#Switch to the new user.
USER ${name}