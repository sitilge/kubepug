# kubepug

A Dockerfile for kubepug, based on Alpine Linux.

## CI/CD

Set up the following values in GitLab CI/CD variables section:

- `CI_REGISTRY_USER` - Docker Hub username.
- `CI_REGISTRY_PASSWORD` - Docker Hub access token or password (not recommended).